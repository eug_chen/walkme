#!/usr/bin/bash

version="1.1"

# set -e stops the execution of a script if a command or pipeline has an error
set -e

if [ "$(id -u)" != "0" ]; then echo "This script must be run as root, exiting..."; exit 3; fi


while true; do
    read -p "Do you wish to Uninstall SL1 tutorial? y/n: " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n.";;
    esac
done


DIR_PATH="/opt/em7/gui/ap/includes"
headerList=(header_liquid.inc header_lite.inc header_modal.inc header_panel.inc)
headerValidater="GSD Customized Interactive Guide - Connect to WalkMe Server"

# Check if the backup for header exists, if not, exit the script.
# Check if validater exists in header, if not, exit the script.  
for file in "${headerList[@]}"
do
    if ! test -f "$DIR_PATH/$file""bak"; then
        echo "Backup file ${DIR_PATH}/${file}bak does not exist. Not able to restore header file.";
        echo "Exit Script."; exit 3;
    fi
    if ! grep -q "$headerValidater" "$DIR_PATH/$file"; then
        echo "'$headerValidater' does not exist in $DIR_PATH/$file.";
        while true; do
            read -p "Do you still want to restore the original header files? y/n: " yn
            case $yn in
                [Yy]* ) break;;
                [Nn]* ) exit;;
                * ) echo "Please answer y or n.";;
            esac
        done
    fi
done


echo -e "\nReplacing header files with backup files..."
for file in "${headerList[@]}"
do
    mv -f $DIR_PATH/$file"bak" $DIR_PATH/$file
    echo "$DIR_PATH/$file updated"
done
echo -e "Replace header files with backup files completed successfully\n"


# echo -e "\nReplacing header files with backup files..."
# mv -f $DIR_PATH/header_liquid.incbak $DIR_PATH/header_liquid.inc
# mv -f $DIR_PATH/header_lite.incbak $DIR_PATH/header_lite.inc
# mv -f $DIR_PATH/header_modal.incbak $DIR_PATH/header_modal.inc
# mv -f $DIR_PATH/header_panel.incbak $DIR_PATH/header_panel.inc
# echo -e "Replace header files with backup files completed successfully\n"

echo -e "SL1 tutorial Uninstalled successfully!\n"