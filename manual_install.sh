#!/usr/bin/bash

function join_by {
  local d=${1-} f=${2-}
  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi
}

version="1.1"
targetSLversion="11.1"

# set -e stops the execution of a script if a command or pipeline has an error
set -e

if [ "$(id -u)" != "0" ]; then echo "This script must be run as root, exiting..."; exit 3; fi

SL1_version=`cat /etc/em7-release |awk {'print $2'} | awk -F"." {'print $1"."$2'}`

if [ "$SL1_version" != "$targetSLversion" ]; then echo "SL1 version not '$targetSLversion'. Current SL1 version $SL1_version, exiting..."; exit 3; fi

while true; do
    read -p "Do you wish to install SL1 tutorial? y/n: " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n.";;
    esac
done


baseURL="https://api.bitbucket.org/2.0/repositories"
bitbucket_PATH="eug_chen/walkme/src/master/includes"
DIR_PATH="/opt/em7/gui/ap/includes"
headerList=(header_liquid.inc header_lite.inc header_modal.inc header_panel.inc)
headerValidater="//GSD Customized Interactive Guide - Connect to WalkMe Server"

getHeaderFromInternet=true
getHeaderFromLocalRepo=true
getHeaderFromIncludeRepo=true
for file in "${headerList[@]}"
do
    # Check if the server are able to reach bitbucket
    if [ "$(curl -s -o /dev/null -w "%{http_code}" $baseURL/$bitbucket_PATH/$file)" -ne "200" ];then 
        # echo "Cannot connect to $baseURL/$bitbucket_PATH";
        getHeaderFromInternet=false; 
    fi;
    # Check if the new header used for replce exists in current repo
    if [[ ! -f "$PWD/$file" ]] || ! grep -q "$headerValidater" "$PWD/$file" ; then
        # echo "No correct file '$file' in local repository. ";
        getHeaderFromLocalRepo=false; 
    fi;
    # Check if the new header used for replce exists in include repo
    if [[ ! -f "$PWD/include/$file" ]] || ! grep -q "$headerValidater" "$PWD/include/$file" ; then
        # echo "Cannot find 'include' repository or no correct file '$file' in include repository. ";
        getHeaderFromIncludeRepo=false; 
    fi;
done

if [ "$getHeaderFromInternet" == false ] && [ "$getHeaderFromLocalRepo" == false ] && [ "$getHeaderFromIncludeRepo" == false ]; then 
    echo "Error: Cannot get modified header: "\'$(join_by "', '" "${headerList[@]}")\'" from $baseURL/$bitbucket_PATH and from local directory or in include directory." ;
    exit 3; 
fi;

# Check if the backup for header already exists  
for file in "${headerList[@]}"
do
    # If no Backup file exists, create one.
    if ! test -e "$DIR_PATH/$file""bak"; then
        echo "Backup files did not exist. Making backup of $DIR_PATH/$file"
        cp $DIR_PATH/$file $DIR_PATH/$file"bak"
    else 
        # If no validater in Header file, It could be that SL1 got pacthed to a newer version and the header file got updated. 
        # Replace the backup file with current header file and then create a new header file.
        if [ "$(file -b --mime-type $DIR_PATH/$file | sed 's|/.*||')" != "text" ]; then
            echo "$DIR_PATH/$file is binary"; 
            while true; do
                read -p "Backup files is probably old due to SL1 upgrade. Do you want to replace backup file $DIR_PATH/${file}bak? y/n: " yn
                case $yn in
                    [Yy]* ) /bin/cp -f  $DIR_PATH/$file $DIR_PATH/$file"bak";break;;
                    [Nn]* ) echo -e "Please check your header file: $file in $DIR_PATH, if it's a binary file, it could be the header file got updated.\nPlease run the script again and replace the old backup file by typing [y]";exit;;
                    * ) echo "Please answer y or n.";;
                esac
            done
        else 
            # If file is not binary but still not have validater in the file
            if ! grep -q "$headerValidater" "$DIR_PATH/$file"; then
            echo "'$headerValidater' does not exist in $DIR_PATH/$file and will be replaced.";
            fi
        fi;
    fi
done

if [ "$getHeaderFromLocalRepo" == true ]; then 
    echo -e "\nUpdating header files from local repository..."
    for file in "${headerList[@]}"
    do
        cp -f  $PWD/$file $DIR_PATH/$file
        echo "$DIR_PATH/$file updated"
    done
elif [ "$getHeaderFromIncludeRepo" == true ]; then 
    echo -e "\nUpdating header files from include repository..."
    for file in "${headerList[@]}"
    do
        cp -f  "$PWD/include/$file" $DIR_PATH/$file
        echo "$DIR_PATH/$file updated"
    done
elif [ "$getHeaderFromInternet" == true ]; then 
    echo -e "\nUpdating header files from bitbucket..."
    for file in "${headerList[@]}"
    do
        curl -s $baseURL/$bitbucket_PATH/$file > $DIR_PATH/$file
        echo "$DIR_PATH/$file updated"
    done
fi;

echo "Update header files complete."

echo -e "SL1 tutorial installed successfully!\n"






