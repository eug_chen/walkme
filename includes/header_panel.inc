<?php
//GSD Customized Interactive Guide - Connect to WalkMe Server
//Send headers first
require_once("$AUTH_sharedir/head_httpheaders.inc");
require_once("$AUTH_funcdir/page_utils.inc");


if (empty($ie_standards_mode)) {
  $doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
} else {
  $doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
}

/////////////////////////ADD SCRIPTS/////////////////////////ADD SCRIPTS/////////////////////////ADD SCRIPTS/////////////////////////ADD SCRIPTS
$js_files = array('jquery','em7_core','thickbox','em7_pops','jquery.form','jquery.flydom','shortcuts','jquery.dom');
if ($exec == "device_tools") {
    $js_files[] = 'hovertip';
}
if ( $act == "my_preferences" || $exec == "account_management")  {
    $js_files[] = 'passwd_check';
}
if ($exec == "device_interfaces_edit" or $act == "admin_system_thresholds") {
    $js_files[] = 'slider';
}
if ($exec == "admin_brand_editor") {
    $js_files[] = 'farbtastic';
}
if ($exec == "account_management") {
    $js_files[] = 'em7_formutils';
}

///////ADDD SHORTSUT KEYS//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!$modal) {
    ///Note: More shortcuts can be passed in by calling page... these are only the defaults.
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+G\",function () { TB_show(\"Guides\", \"/em7/index.em7?exec=guides&rnd=$AUTH_rand&height=500&width=750&TB_iframe=true\", \"\", \"\"); },{'type':'keydown','propagate':true,'target':document})";
    //Bookmarks should not use modals when outside of main page... causes target issues.
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+B\",function () { window.open('/em7/index.em7?exec=bookmarks&rand=$AUTH_rand&Book_URL=$AUTH_uri_encoded&rnd=$AUTH_rand', 'bookmark','width=731,height=446,top=350px,left=400px,location=no,menubar=no,scrollbars=no,resizable=no,status=no'); },{'type':'keydown','propagate':true,'target':document}) ";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

print("$doctype
<!--Copyright (c) ScienceLogic, Inc.  All rights reserved.-->
<html>
<head>
<link rel=\"shortcut icon\" href=\"/em7/favicon.ico?v=755\" type=\"image/x-icon\"/>
<title>$branded_name</title>

<meta http-equiv=\"Cache-Control\" content=\"no-cache\" />
<meta http-equiv=\"Pragma\" content=\"no-cache\" />
<meta http-equiv=\"Expires\" content=\"-1\" />
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
<meta http-equiv='MSThemeCompatible' content='yes' />
<meta http-equiv='imagetoolbar' content='No' />
<meta name='MSSmartTagsPreventParsing' content='yes' />
<meta name='robots' content='noindex, nofollow' />
<meta name='copyright' content='&copy; 2003 - $AUTH_year ScienceLogic, Inc. All Rights Reserved' />
<meta name='author' content='ScienceLogic, Inc.' />
<meta name='language' content='en-us' />
<meta name='random_key' content='$AUTH_rand' />
");
page_echo_js_links($js_files);
require_once($AUTH_sharedir.'/header_csrf.inc');
print("
<script language='javascript' type=\"text/javascript\">
  var UNID = \"$_SESSION[id]\";
  window.focus();
");
if (is_array($ShortCutArray)) {
    for ($x=0; $x < count($ShortCutArray); $x++) {
        echo "  $ShortCutArray[$x]\n";
    }
}
$inhibit_span = (empty($inhibit_span_styles)) ? NULL : '?inhibit_span_styles=1';
print("</script>

<link rel=\"stylesheet\" type=\"text/css\" href=\"/em7/libs/css.em7/$AUTH_my_brand/style_classic$inhibit_span\" />

");

if (!empty($include_treeview_files)) {
    page_echo_css_links('jquery.treeview');
    page_echo_js_links('jquery.treeview');
}

// Change the styles for the CP
if(!empty($_GET['cpe'])){
  ?>
  <script type="text/javascript">
  $(function(){
    $('body').css('background','0');
    $('#panel_body').css('padding','0px');
  });
  </script>
<?php
}

print("
</head>
<body class='em7 main' onhelp=\"return false;\" $ONSTATEMENT>
<div id=\"overDiv\" style=\"position:absolute; visibility:hidden; z-index:10000; height: auto;\"></div>
<div class='panel_body' id='panel_body'>
<script type='text/javascript'> (function() { var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/11d39bd684584e869ac4d42d3e79d71c/test/walkme_11d39bd684584e869ac4d42d3e79d71c_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig = {smartLoad:true}; })(); </script>
");

?>