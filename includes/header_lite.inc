<?php
//GSD Customized Interactive Guide - Connect to WalkMe Server
//Send headers first
require_once("$AUTH_sharedir/head_httpheaders.inc");
require_once("$AUTH_funcdir/page_utils.inc");

$window_title = $branded_name;
if (!empty($em7_page_title)) {
  $window_title = "$em7_page_title - $window_title";
}

if (empty($ie_standards_mode)) {
  $doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
} else {
  $doctype = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
}

$js_files = array('jquery','em7_core','thickbox','em7_pops');
if (conf_getValue('access_management') == '1' and conf_getValue('enable_whitelisting') == '1') {
  $js_files[] = 'access_manager_preload';
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

print("$doctype
<!--Copyright (c) ScienceLogic, Inc.  All rights reserved.-->
<html>
<head>
<link rel=\"shortcut icon\" href=\"/em7/favicon.ico?v=755\" type=\"image/x-icon\"/>
<title>".htmlspecialchars($window_title)."</title>

<meta http-equiv=\"Cache-Control\" content=\"no-cache\" />
<meta http-equiv=\"Pragma\" content=\"no-cache\" />
<meta http-equiv=\"Expires\" content=\"-1\" />
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
<meta http-equiv='MSThemeCompatible' content='yes' />
<meta http-equiv='imagetoolbar' content='No' />
<meta name='MSSmartTagsPreventParsing' content='yes' />
<meta name='robots' content='noindex, nofollow' />
<meta name='copyright' content='&copy; 2003 - $AUTH_year ScienceLogic, Inc. All Rights Reserved' />
<meta name='author' content='ScienceLogic, Inc.' />
<meta name='language' content='en-us' />
<meta name='random_key' content='$AUTH_rand' />
");
page_echo_js_links($js_files);
require_once("$AUTH_sharedir/header_csrf.inc");
print("
<script language='javascript' type=\"text/javascript\">
    var UNID = \"$_SESSION[id]\";
</script>
");

if (empty($body_class)) $body_class = 'em7';

if (empty($leave_head_open)) {
print("
<link rel=\"stylesheet\" type=\"text/css\" href=\"/em7/libs/css.em7/$AUTH_my_brand/style_classic\" />

</head>
<body class='$body_class' bottomMargin=0 leftMargin='0' topMargin='0' rightMargin='0' marginwidth='0' marginheight='0'>
<script type='text/javascript'> (function() { var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/11d39bd684584e869ac4d42d3e79d71c/test/walkme_11d39bd684584e869ac4d42d3e79d71c_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig = {smartLoad:true}; })(); </script>
");
}
?>