<?php
//GSD Customized Interactive Guide - Connect to WalkMe Server
require_once EM7_PATH_PHPSHARE.'/functions/util/ap2.inc';

if (!array_key_exists('olh', $_GET) and ap2_embedded()) {
  return require_once __DIR__.'/header_ap2_embedded.inc';
}

require_once("$AUTH_sharedir/head_httpheaders.inc");
require_once("$AUTH_funcdir/page_utils.inc");
require_once("$AUTH_siloroot/phpshare/functions/util/globalmanager.inc");


////////////////////////MAKE MENU AND SHORTCUTS ARRAYS///////////////////////////////////////////////////////////////////////////////

require_once("$AUTH_sharedir/functions_tabs.inc");

///////////////SHORTCUTS

$shortcuts = array();
$tabs = array();
$menu_items_main = array();

  // Here Lie The Credits. EM-6352 Remove Credits from Product

  ///Default guides
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+G\",function () { TB_show(\"Guides\", \"/em7/index.em7?exec=guides&rnd=$AUTH_rand&height=500&width=750&TB_iframe=true\", \"\", \"\"); },{'type':'keydown','propagate':true,'target':document});";
  ///Home Page
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+H\",function () { location.assign(\"/em7/\"); },{'type':'keydown','propagate':true,'target':document});";
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+dot\",function () { location.assign(\"/em7/\"); },{'type':'keydown','propagate':true,'target':document});";
  ///Logout
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+X\",function () { LogOff('/em7/logout.em7?roa_id=0', 'sign-out right now?'); },{'type':'keydown','propagate':true,'target':document});";
  ////FINDER
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+F\",function () { TB_show(\"Finder\", \"/em7/index.em7?exec=select_entity_ajax&rnd=$AUTH_rand&height=530&width=535&TB_iframe=true\", \"\", \"\"); },{'type':'keydown','propagate':true,'target':document});";
  ///Bookmarks
  $ShortCutArray[]="shortcut(\"Ctrl+Alt+B\",function () { TB_show(\"Bookmarks\", \"/em7/index.em7?exec=bookmarks&modal=1&rand=$AUTH_rand&Book_URL=$AUTH_uri_encoded&height=400&width=700&TB_iframe=true\", \"\", \"\"); },{'type':'keydown','propagate':true,'target':document});";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if (access_hooks::obj()->check('SYS_IBOX_VIEW')) {
    $tabs[] = array('?exec=my_ibox', '&Inbox');
  }

  if (access_hooks::obj()->check('DASH_VIEW')) {
    $tabs[] = array('?exec=dashboards', '&Dashboards');
  }

  if (access_hooks::obj()->check('VIEWS_VIEW')) {
    $tabs[] = array('?exec=maps', '&Views');
  }

  $gm_events = globalmanager_installed();
  if (access_hooks::obj()->check('EVT_VIEW') AND !$gm_events) {
    $tabs[] = array('?exec=events', '&Events');
  }
  if (access_hooks::obj()->check('EVT_VIEW') AND $gm_events) {
    $tabs[] = array("?exec=nav_container&c=event-console", '&Events');
  }

  if (access_hooks::obj()->check('TKT_CONSOLE_VIEW')) {
    $tabs[] = array('?exec=ticket_management', '&Tickets');
  }

  if (access_hooks::obj()->check('RPRT_VIEW')) {
    $tabs[] = array('?exec=reporting', 'Rep&orts');
  }

  if (access_hooks::obj()->check('SYS_REGISTRY_PAGE')) {
    $tabs[] = array('?exec=registry', '&Registry');
  }

  if (access_hooks::obj()->check('SYS_SYSTEM_PAGE')) {
    $tabs[] = array('?exec=admin', '&System');
  }

  if (access_hooks::obj()->check('ACT_MY_PAGE')) {
    $tabs[] = array('?exec=my', '&Preferences');
  }

////////////////TOOLBOX////////////////TOOLBOX////////////////TOOLBOX////////////////TOOLBOX

  //$MenuArray[]="<td class=topnav><button id='tools_main' type=submit onmouseover=\"this.className='Hbuttonon'\" onfocus=\"this.className='Hbuttonon'\" onblur=\"this.className='HbuttonM'\" onmouseout=\"this.className='HbuttonM'\" class=\"HbuttonM flyout_trigger\">Tools</button></td>";


  $menu_items_main[]=array("Bookmarks (Ctrl+Alt+B)", "/em7/index.em7?exec=bookmarks&modal=1&rand=$AUTH_rand&Book_URL=$AUTH_uri_encoded&height=400&width=700&TB_iframe=true", "Bookmarks", "thickbox");
  //Create a ticket
  if (!system_settings::field('hide_ticket_create_in_toolbox') and access_hooks::obj()->check('TKT_CREATE')) {
    $menu_items_main[]=array("Create a Ticket (Ctrl+Alt+Enter)", "/em7/index.em7?exec=ticket_editor", "Create a Ticket", "", "ticketing", "zoombox");
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+Enter\",function () { window.open('/em7/index.em7?exec=ticket_editor','ticketing','width=985,height=795,left=10,top=10,location=no,menubar=no,scrollbars=no,resizable=no,status=no'); },{'type':'keydown','propagate':true,'target':document});";
  }
  if (access_hooks::obj()->check('SYS_FINDER')) {
    $menu_items_main[]=array("Finder (Ctrl+Alt+F)", "/em7/index.em7?exec=select_entity_ajax&rnd=$AUTH_rand&height=530&width=535&TB_iframe=true", "Finder", "thickbox");
  }
  $menu_items_main[]=array("Guides (Ctrl+Alt+G)", "/em7/index.em7?exec=guides&rnd=$AUTH_rand&height=500&width=750&TB_iframe=true", "Guides", "thickbox");
  $menu_items_main[]=array("Home (Ctrl+Alt+H)", "/em7/");
  if (access_hooks::obj()->check('ACT_MY_PREFERENCES_PAGE')) {
    $menu_items_main[]=array("My Preferences (Ctrl+Alt+P)", "/em7/index.em7?exec=my_preferences_ajax&rnd=$AUTH_rand&height=500&width=975&TB_iframe=true", "My Preferences", "thickbox");
  }
  if (access_hooks::obj()->check('TKT_CONSOLE_VIEW')) {
    $menu_items_main[]=array("My Tickets (Ctrl+Alt+M)", "/em7/index.em7?exec=ticket_management&q_assigned=".authuser::uid());
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+M\",function () { location.assign(\"/em7/index.em7?exec=ticket_management&q_assigned=".authuser::uid()."\"); },{'type':'keydown','propagate':true,'target':document});";
  }
  $menu_items_main[] = array("Regular Expression Tester", "/em7/index.em7?exec=regextest&height=300&width=400&TB_iframe=true", "Regular Expression Tester", "thickbox");
  if (hooks()->check('SYS_TOOLS_CACHE_MGMT')) {
    $menu_items_main[] = array("Clear SL1 System Cache", "?exec=admin_cache_clear_ajax&height=20&width=300", "Clear Cache", "thickbox");
  }
  $menu_items_main[] = NULL;

  if (access_hooks::obj()->check('SYS_DISCOVERY_PAGE')) {
    $menu_items_main[]=array("Auto-Discovery (Ctrl+Alt+A)", "/em7/index.em7?exec=admin&act=admin_auto_discovery_ipv6&rand=$AUTH_rand");
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+A\",function () { location.assign(\"/em7/index.em7?exec=admin&act=admin_auto_discovery_ipv6&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
  }

//removed because sometime less is more...
//
//  Xtype  Xtype_name
//      0 Organization
//      1 Device
//      2 Asset
//      4 IP Network
//      5 Interface
//      6 Vendor
//      7 User Account
//      8 Virtual Interface
//      9 Device Group

  //Organizational Management
  if (access_hooks::obj()->check('ORG_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+0\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_organizations&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("Organizational Management (Ctrl+Alt+0)", "/em7/index.em7?exec=registry&act=registry_organizations&rand=$AUTH_rand");
  }

  //Device Management
  if (access_hooks::obj()->check('DEV_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+1\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_device_management&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("Device Management (Ctrl+Alt+1)", "/em7/index.em7?exec=registry&act=registry_device_management&rand=$AUTH_rand");
  }

  //Asset Management
  if (access_hooks::obj()->check('AST_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+2\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_assets&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("Asset Management (Ctrl+Alt+2)", "/em7/index.em7?exec=registry&act=registry_assets&rand=$AUTH_rand");
  }

  //Network IP Management
  if (access_hooks::obj()->check('IP_NETWORKS_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+3\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_interfaces\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("Network IP Management (Ctrl+Alt+3)", "/em7/index.em7?exec=registry&act=registry_interfaces&rand=$AUTH_rand");
  }

  //User Accounts
  if (access_hooks::obj()->check('ACT_USER_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+4\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_users&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("User Accounts (Ctrl+Alt+4)", "/em7/index.em7?exec=registry&act=registry_users&rand=$AUTH_rand");
  }

  //Vendor Management
  if (access_hooks::obj()->check('ACT_VENDOR_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+5\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_vendors&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("Vendor Management (Ctrl+Alt+5)", "/em7/index.em7?exec=registry&act=registry_vendors&rand=$AUTH_rand");
  }

  //recipients Management
  if (access_hooks::obj()->check('ACT_EMAIL_SUB_VIEW')) {
    $ShortCutArray[]="shortcut(\"Ctrl+Alt+6\",function () { location.assign(\"/em7/index.em7?exec=registry&act=registry_recipients&rand=$AUTH_rand\"); },{'type':'keydown','propagate':true,'target':document});";
    $menu_items_main[]=array("External Contact Management (Ctrl+Alt+6)", "/em7/index.em7?exec=registry&act=registry_recipients&rand=$AUTH_rand");
  }

  // 3rd Party License info dialog
  $menu_items_main[] = NULL;
  $menu_items_main[]=array("License Information", "licenseinfo");

  // Version info dialog
  $menu_items_main[]=array("SL1 Version Information", "versioninfo");

  // Log off
  $menu_items_main[] = NULL;
  $menu_items_main[]=array("LOG OFF", "/em7/logout.em7");

  $login_info = core_display_last_login(authuser::obj()->uid);


$tabs = array_merge($tabs, tabs_get_simple(1, $AUTH_account_type));
$tablinks = tabs_to_links($tabs, $ShortCutArray, false);

$logo_hover_text = "SL1 - $AUTH_release"
                 . "<hr class=logo_hr>"
                 . "Copyright © 2003 - $AUTH_year ScienceLogic, Inc. All Rights Reserved.<br>"
                 . "ScienceLogic® is a registered trademark of ScienceLogic, Inc."
                 . "<hr class=logo_hr>"
                 . "SL1 and technologies contained herein are patent pending.";

$js_files = array('jquery','em7_core','thickbox','em7_pops','jquery.form','jquery.flydom','jquery.dom','shortcuts','HashLocation');
$css_files = array_merge((array) @$css_ext, array('em7_base','thickbox','registry_table','absolute_dialog','em7_main'));
// Some webkit browsers like to pretend that the mouse has moved to 0,0 when a dropdown is clicked.
// So don't fire the tooltip if the mouse is at 0,0.

//if user is admin show em7 info
$js_init = '
  jq_pseudohover(".button");
  jq_flyout_hook(); '
  .((!authuser::is_admin())?'': '$("#em7_logo").hover(function(evt) {if (evt.pageX != 0 || evt.pageY != 0) ToolTips('.json_encode($logo_hover_text).', CAPTION, "SL1 VERSION INFORMATION", DELAY, "1500", BELOW);}, nd);')
  .' $("#em7_close_mainnav").click(function(evt){
    evt.preventDefault();
    HashLocation.setVar("em7_mainnav", "hide", true);
    $("#em7_mainnav").hide();
    $("#em7_maincontent").animate({top:0});
    $("#expiration_display").hide();
    $("#em7_open_mainnav").show();
  });
  $("#em7_open_mainnav").click(function(evt){
    evt.preventDefault();
    HashLocation.setVar("em7_mainnav", null, true);
    $("#em7_mainnav").show();
    $("#em7_maincontent").animate({top:'.(thm('compact_header')?72:105).'});
    $("#expiration_display").show();
    $(this).hide();
  });
  if (HashLocation.getVars().em7_mainnav == "hide") {
   $("#em7_mainnav").hide();
   $("#em7_maincontent").css("top",0);
   $("#expiration_display").hide();
   $("#em7_open_mainnav").show();
  }
  $("#em7_finder_quick").submit(function(evt){
    evt.preventDefault();
    TB_show("Finder",this.action+"?"+$(this).serialize()+"&height=530&width=535&TB_iframe=true","","");
  });
  ';

  //add license info in a modal
  $js_init .= " $(function(){

    $('a[href=licenseinfo]').click(function(event){
        event.preventDefault();
        TB_show('License Information','/em7/license.html?width=1000&height=600"
          . "&color=" . urlencode($branded_white)
          . "&hover_bg=" . urlencode($branded_navlight1)
          . "&hover_fg=" . urlencode($branded_white)
          . "&vscroll=600&TB_iframe=true');
      });

  });
    ";

  //add version info in a modal
  $js_init .= " $(function(){
    vid = $('<div/>',{id:'version_info_display',
                        style:'position:fixed;z-index:1000;padding:2px;top:25px;left:40%;border:2px solid $branded_color3;width:221px;height:12px;display:none;',
                        'class': ''});
                        vid.append('<div>'+'$logo_hover_text'+'</div>');
    $('body').append(vid);
    $('a[href=versioninfo]').click(function(event){
        event.preventDefault();TB_show('SL1 Version Information','?inlineId=version_info_display&width=420&height=150&TB_inline=true');
      });

  });
    ";
if (authuser::is_admin()) {
  $session_hover_text = "System Time: $AUTH_formal_date"
                      . "<hr style='width: 100%; background-color: $branded_color3; color: $branded_color3; height: 1px;'>"
                      . "<u>Build:</u>SL1 $AUTH_release<br>"
                      . "<u>Agent:</u>$HTTP_USER_AGENT<br>"
                      . "<u>Cookie:</u>$PHPSESSID<br>"
                      . "<u>System:</u>$SERVER_SOFTWARE";
  $js_init .= '$("#session_over").css("cursor","help").hover(function() {ToolTips('.json_encode($session_hover_text).', DELAY, "6000", CAPTION, "SESSION INFORMATION");}, nd);';
}
foreach ($ShortCutArray as $shortcut) {
  $js_init .= $shortcut."\n";
}

if (isset($obj_navbartree)) {
  $js_files[] = 'jquery.treeview';
  $css_files[] = 'jquery.treeview';
  if (authuser::obj()->navbar_autohide_disabled) {
    $js_init .= '
      $("#em7_close_subnav").click(function(evt){
        evt.preventDefault();
        HashLocation.setVar("em7_subnav", "hide", true);
        $("#em7_subnav_container").animate({width:2});
        $("#em7_subcontent").animate({left:2});
        $("#em7_open_subnav").show();
      });
      $("#em7_open_subnav").click(function(evt){
        evt.preventDefault();
        HashLocation.setVar("em7_subnav", null, true);
        $("#em7_subnav_container").animate({width:182});
        $("#em7_subcontent").animate({left:182});
        $(this).hide();
      });
      if (HashLocation.getVars().em7_subnav == "hide") {
        $("#em7_subnav_container").css("width",2);
        $("#em7_subcontent").css("left",2);
        $("#em7_open_subnav").show();
      }
    ';
  } else {
    $js_init .= '
      $("#em7_subnav_tab").mouseenter(function(){
        var cont = $("#em7_subnav_container");
        if (cont[0].isOpening || cont[0].isClosing || cont[0].isOpen) return;
        cont[0].isAnimating = cont[0].isOpen = true;
        cont.animate({width:200}, function() {cont[0].isOpening = false;})
          .hover(function() {
            if (this.closeTimeout) window.clearTimeout(this.closeTimeout);
            this.closeTimeout = null;
          }, function() {
            this.closeTimeout = window.setTimeout(function() {
              cont[0].isClosing = true;
              cont[0].isOpen = false;
              cont.animate({width:18}, function() {cont[0].isClosing = false;})
                  .unbind("mouseenter mouseleave");
            }, 500);
          });
      });
    ';
  }
  $js_init .= '
    $("#navbartree").treeview({
      animated: 100,
      collapsed: true
    });
    $("#treeview_findme").keyup(treeview_find);
    $("#findmebtn").click(treeview_find);
  ';
}
if (conf_getValue('LOCAL','access_management') and conf_getValue('LOCAL','enable_whitelisting')) {
  $js_files[] = 'access_manager_preload';
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$page_open = "<div id='em7_content_area'><div id='em7_content_bg'><div id='em7_page_content'>";
$page_footer = "</div></div></div>";

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--Copyright (c) ScienceLogic, Inc.  All rights reserved.-->
<html>
<head>
<link rel="shortcut icon" href="/em7/favicon.ico?v=755" type="image/x-icon"/>
<title><?=$branded_name?></title>

<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
<meta http-equiv='MSThemeCompatible' content='yes' />
<meta http-equiv='imagetoolbar' content='No' />
<meta name='MSSmartTagsPreventParsing' content='yes' />
<meta name='robots' content='noindex, nofollow' />
<meta name='copyright' content='&copy; 2003 - <?=$AUTH_year?> ScienceLogic, Inc. All Rights Reserved' />
<meta name='author' content='ScienceLogic, Inc.' />
<meta name='language' content='en-us' />
<meta name='random_key' content='<?=$AUTH_rand?>' />

<? page_echo_js_links($js_files) ?>
<script type='text/javascript'>
var UNID = <?=json_encode($_SESSION['id'])?>;
<? if ($js_init): ?>
$(function() {
  <?=$js_init?>
});
<? endif ?>

<? admin_notifier_banner_phtml(); ?>
<? if(isset($_SESSION['autodetect_timezone']) && $_SESSION['autodetect_timezone'] == 0) :?>
  <? if(!isset($_SESSION['timezone_message_hide_flag'])):?>
    <? include "$AUTH_sharedir/autodetect_timezone_message.inc"; ?>
    <? $_SESSION['timezone_message_hide_flag'] = 1?>
  <? endif?>
<? endif?>
<?=display_successful_login_alert($_SESSION['firstlogin']) ?>

</script>
<? page_echo_css_links($css_files) ?>
</head>
<body>
  <div id='em7_mainnav'>
    <img id='em7_logo' src='<?=$branded_logo?>' />
    <div id='em7_info_header'>


        <div>
          <div class='info_line' style="font-size: 120%; overflow: hidden; white-space: nowrap;" title="<?=str_replace('"', "'", $AUTH_user)?> from <?=$REMOTE_ADDR?> in <?=$AUTH_timezone_descr?>">
            <span class="navbar_user_icon"></span>
            <!-- <i class="fa fa-user" style="vertical-align:top; padding-top:2px; font-size:14px;"></i> --> <span id='em7_user_info' style="vertical-align:top">Logged in as</span> <span id='em7_user_info' style="font-weight: bold; vertical-align:top;"><?=$AUTH_user?></span>
            <a href='#em7_tools_main_flyout' style="color:inherit;  " id='em7_tools_main' class='flyout_trigger' title='Click to see the toolbox'><span class="navbar_bars_icon"></span><!-- <i class="fa fa-bars" style="font-size: 130%;"></i> --></a>
              <? page_echo_flyout_menu('em7_tools_main', $menu_items_main, 'right', 'below') ?>
          </div>

          <div class='info_line'>
            <? if (access_hooks::obj()->check('SYS_FINDER')): ?>
              <form id='em7_finder_quick' method='get' action='/em7/index.em7'>
                <input type='hidden' name='exec' value='select_entity_ajax' />
                <input type='hidden' name='execute_finder' value='1' />
                <div>
                  <div style="width:auto; float:left; margin-top: -8px; margin-right: 0px;">Finder
                  <input name='q_arg' type='text' id='em7_finder_search' style="line-height: 14px;"/>&nbsp;<input type='submit' id='em7_finder_submit' class='tiny button' value=' Go ' />
                  </div>
                </div>
              </form>
            <? endif ?>
          </div>
        </div>
    </div>

    <div class='tabbar'>
      <? foreach ($tablinks as $tab): ?>
      <?=$tab?>
      <? endforeach ?>
    </div>
    <a id='em7_close_mainnav' class='grab_handle_u' href='#em7_maincontent'></a>
  </div>
  <div id='em7_maincontent'>
    <a id='em7_open_mainnav' class='grab_handle_d' href='#em7_mainnav'></a>
    <? if (isset($obj_navbartree)): ?>
    <div id='em7_subnav_container' <?=authuser::obj()->navbar_autohide_disabled?NULL:"class='auto_hide'"?>>
      <div id='em7_subnav'>
        <div id='em7_subnavtree'>
          <? $obj_navbartree->echo_ul_html() ?>
        </div>
        <div id='em7_subnavsearch'>
          <input type='text' id='treeview_findme' autocomplete='off' />
          <input type='button' class='short button' value='Find' id='findmebtn' disabled='disabled' />
        </div>
        <a id='em7_close_subnav' class='grab_handle_l' href='#em7_subcontent'></a>
      </div>
      <? if (!authuser::obj()->navbar_autohide_disabled): ?>
      <div id='em7_subnav_tab'></div>
      <? endif ?>
    </div>
    <div id='em7_subcontent'>
      <a id='em7_open_subnav' class='grab_handle_r' href='#em7_subnav'></a>
      <? $page_footer .= "
    </div>" ?>
    <? endif ?>
    <?=$page_open?>
    <? $page_footer .= "
  </div>
  <div id='em7_copyright'>$branded_copy</div>
  <div id='em7_logininfo'>$login_info</div>
<script type='text/javascript'> (function() { var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/11d39bd684584e869ac4d42d3e79d71c/test/walkme_11d39bd684584e869ac4d42d3e79d71c_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig = {smartLoad:true}; })(); </script>
</body>
</html>" ?>
<? return $page_footer ?>