# Install WalkMe Interactive Guide in SL1

To install WalkMe interactive guide in a SL1 server, please follow the steps below.

**Grant root access before install WalkMe on SL1, execute `sudo -s` and login with password.**

## Installation
### Install directly using the command below:

```bash
bash <(curl -s https://api.bitbucket.org/2.0/repositories/eug_chen/walkme/src/master/manual_install.sh)
```

### Or the alternative ways:
1. Download **manual_install.sh** using wget:
    ```bash
    wget https://api.bitbucket.org/2.0/repositories/eug_chen/walkme/src/master/manual_install.sh
    ```
2. Change permission of the installation file to executable:
    ```bash
    sudo chmod +x manual_install.sh
    ```
3. And execute the install file:
    ```bash
    sudo ./manual_install.sh
    ```

### If you need to install with no internet access
1. Download [repository](https://bitbucket.org/eug_chen/walkme/downloads/) manually.
2. Unzip the file and upload the directory to SL1 server.
3. SSH to the SL1 server and navigate into the directory you just uploaded. 
4. Find and navigate to the 'manual_install.sh' in the directory. 
    - For example: `cd /home/em7admin/eug_chen-walkme-fff8608f7848/eug_chen-walkme-fff8608f7848`
4. Change permission of the installation file to executable:
    ```bash
    sudo chmod +x manual_install.sh
    ```
5. And execute the install file:
    ```bash
    sudo ./manual_install.sh
    ```

## Uninstall
### Uninstalldirectly using the command below:
```bash
bash <(curl -s https://api.bitbucket.org/2.0/repositories/eug_chen/walkme/src/master/manual_uninstall.sh)
```
### Or the alternative ways:
1. Download **manual_uninstall.sh** using wget:
    ```bash
    wget https://api.bitbucket.org/2.0/repositories/eug_chen/walkme/src/master/manual_uninstall.sh
    ```
2. Change permission of the uninstallation file to executable:
    ```bash
    sudo chmod +x manual_uninstall.sh
    ```
3. And execute the uninstall file:
    ```bash
    sudo ./manual_uninstall.sh
    ```